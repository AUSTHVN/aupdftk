AuPDFtk
=======

A set of PHP classes for working with PDF forms via PDFtk.

*Part of a collection of utilities, classes, etc. that are developed as needed and keep for possible future use.*

## Features

* Pull form field metadata from PDF form
* Parse through field data to auto-generate a basic form HTML
* Fill out a PDF form using data received from an HTML form
* Save fill-out forms to a specific directory
* Server temporary directories are used to store data until the `save` method is invoked on an AuPDFtk object

## Issues

* Output forms, even when not flattened by pdftk, don't appear editable (except Signature fields) in Adobe Reader. However, if you open the PDF in the brownser (e.g. with Chrome), all of the fields *except Signatures* remain editable as expected.
* PDFtk will not dump_data_fields on "dynamic XML" PDF forms; they must be saved as static.
* Be careful when creating PDF forms and giving fields friendly names, alt names, and options (radio buttons, for example). *Default/template LiveCycle forms do not do this and are a complete pain to work with.*
* PDFtk needs the password for forms that are protected in order to function.

## Useage

`require_once` the file(s) as needed.

## Working On...

* Checking PHP version compatibility (trying for 5.3 and up)
* PSR-2, 3, and 4 standardization
* Composer packaging

## Tested under...

* PHP 5.6
* PDFtk Server 2.01
