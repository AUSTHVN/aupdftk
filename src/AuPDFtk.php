<?php namespace AUSTHVN\AugustTools;

/**
 * A class for working PDFtk Server
 *
 * This class is designed to make working with PDFtk Server via PHP a lot more manageable.
 * Adapted from a Sitepoint tutorial ({@link https://www.sitepoint.com/filling-pdf-forms-pdftk-php/}) as needed.
 *
 * @version 0.5.0
 */
class AuPDFtk
{

    public function __construct( $pdfurl, $data = null )
    {
        $this->pdfurl = $pdfurl;
        $this->data   = $data;
    }

    public function get( $property )
    {
        return $this->$property;
    }

    public function set( $property, $value )
    {
        $this->$property = $value;
    }

    /**
     * Uses PDFtk to extract the form fields from the PDF form itself.
     *
     * This is a utility function that isn't actually used in the process of generating the output form.
     * It is design to print out the form fields for reference.
     *
     * @param boolean $readable Set to true to return human-readable string
     *
     * @return string Returns names of PDF form fields for reading/reference.
     */
    public function fields( $readable = FALSE )
    {
        $tmpFile = $this->tmpFile();

        exec( "pdftk {$this->pdfurl} dump_data_fields > {$tmpFile}" );
        $contents = file_get_contents( $tmpFile );

        unlink( $tmpFile );
        return $readable == TRUE ? nl2br( $contents ) : $contents;
    }

    /**
     * Parses PDF form field meta data
     *
     * Hopefully this will be used to generate a form dynamically.
     *
     * @return array
     */
    public function parseFieldMeta( $ignore = FALSE )
    {
        $fieldData      = $this->fields();
        // PDFtk returns the metadata for each field separated by '---'
        $fieldDataArray = explode( '---', $fieldData );
        // The first charactres of the return data are '---'
        unset( $fieldDataArray[0] );
        $fieldDataArray = array_values( $fieldDataArray );
        foreach ( $fieldDataArray as $k => $v ) {
            // Colon at end of line (:\n+)
            // New line at end of string (?=$\n+)
            $fieldDataArray[$k] = preg_replace( '/(:\n)/', PHP_EOL, $v );
            $properties = explode( PHP_EOL, $fieldDataArray[$k] );
            $formField  = new \stdClass();
            foreach ( $properties as $prop ) {
                if ( !empty( $prop ) ) {
                    $tmpArr = preg_split( '/(:\s+)/', $prop );
                    if ( $ignore && in_array( $tmpArr[0], $this->ignoreMeta ) ) continue;
                    // Certain fields have multiple possible values as FieldStateOption
                    // We'll have to make an array out of those.
                    if ( $tmpArr[0] == "FieldStateOption" ) $formField->FieldStateOption[] = $tmpArr[1];
                    else $formField->$tmpArr[0] = $tmpArr[1];
                }
            }

            $fieldDataArray[$k] = $formField;
        }
        return $fieldDataArray;
    }

    /**
     * Set the 'Flatten' attribute
     * @return string
     */
    public function flatten()
    {
        $this->flatten = ' flatten';
        return $this;
    }

    /**
     * Saves output PDF to specified location
     *
     * Saving the output copies it from the system temp direction to the desired location
     * and updates the $output property accordingly, returning the updated class AuPDFtk object.
     *
     * If the destination folder doesn't exit, this function attempts to create it.
     * @todo Consolidate checking for output file into private function
   *
     * @param  string   $path Path to where output PDF should be saved
     *
     * @return AuPDFtk  Returns the AuPDFtk object with updated output value that points to saved file
     */
    public function save( $path = null )
    {
        if ( is_null( $path ) ) return $this;
        if ( !$this->output ) $this->generate();

        $destination = pathinfo( $path, PATHINFO_DIRNAME );
        if ( !file_exists( $destination ) ) mkdir( $destination, 0775, TRUE );

        copy( $this->output, $path );
        unlink( $this->output );

        $this->output = $path;

        return $this;
    }

    /**
     * Force the browser to download the output PDF
     * @todo Consolidate checking for output file into private function
     * @return void
     */
    public function download()
    {
        if ( !$this->output ) throw new \Exception( 'No output PDF found; it must first be generated!' );

        $filepath = $this->output;
        if ( file_exists( $filepath ) ) {
            header( 'Content-Description: File Transfer' );
      header( 'Content-Type: application/pdf' );
      header( 'Content-Disposition: attachment; filename=' . uniqid( gethostname() ) . '.pdf' );
      header( 'Expires: 0' );
      header( 'Cache-Control: must-revalidate' );
      header( 'Pragma: public' );
      header( 'Content-Length: ' . filesize( $filepath ) );

      readfile( $filepath );

      exit;
        }
    }

    /**
     * Path to raw PDF form
     * @var string
     */
    private $pdfurl;

    /**
     * Form data
     * @var array
     */
    private $data;

    /**
     * Path to filled PDF form
     * @var string
     */
    private $output;

    /**
     * Toggle for flattening the output PDF
     * @var string
     */
    private $flatten;

    /**
     * Extracted and processed metadata of form fields
     * @var array
     */
    private $fieldMeta;

    /**
     * Field metadata to ignore
     *
     * These properties will be dumped in parseFieldMeta()
     * @var array
     */
    private $ignoreMeta = [ 'FieldFlags', 'FieldJustification' ];

    /**
     * Generate FDF file
     *
     * This function iterates over the supplied data array to create an FDF file for PDFtk to actually populate the PDF form with web form data.
     * A specific format must be maintained in order for the FDF file to be valid.
     *
     * @param array $data An array of form data, typically $this->data
     *
     * @return string Returns the path to the FDF file.
     */
    private function makeFdf( $data )
    {
        $fdfData = '%FDF-1.2
        1 0 obj<</FDF<< /Fields[';

        foreach ( $data as $k => $v ) {
            // Don't send empty data, it should allow it to be added later
            if ( empty( $v ) ) continue;
            $fdfData .= '<</T(' . $k . ')/V(' . $v . ')>>';
        }

        $fdfData .= '] >> >>
        endobj
        trailer
        <</Root 1 0 R>>
        %%EOF';

        $fdfFile = $this->tmpFile();
        file_put_contents( $fdfFile, $fdfData );

        return $fdfFile;
    }

    /**
     * Use FDF file to complete PDF form
     */
    private function generate()
    {
        $fdfFile      = $this->makeFdf( $this->data );
        $this->output = $this->tmpFile();
        exec( "pdftk {$this->pdfurl} fill_form {$fdfFile} output {$this->output}{$this->flatten}" );
        unlink( $fdfFile );
    }

    /**
     * Creates temp files for PDFtk to use
     */
    private function tmpFile()
    {
        return tempnam( sys_get_temp_dir(), gethostname() );
    }

}
