<?php namespace AUSTHVN\AugustTools;

/**
 * Separate class for taking parsed PDFtk form field metadata and generating an HTML form automatically.
 *
 * @todo Merge into AuPDFtk class to consolidate everything nicely.
 */
class AuPDFtkForm
{
    // public function __construct( $pdfForm, $metadata )
    public function __construct( $metadata )
    {
        // if ( !( $pdfForm instanceof AuPDFtk ) && !file_exists( $pdfForm ) )
        //  throw new \InvalidArgumentException( 'Supplied PDF Form object must be of type AuPDFtk or a valid filepath!' );
        if ( !is_array( $metadata ) )
            throw new \InvalidArgumentException( 'Supplied metadata must be an array of stdClass objects!' );
        // $this->pdfForm    = ( $pdfForm instanceof AuPDFtk ) ? $pdfForm->get( 'pdfurl' ) : $pdfForm;
        $this->formfields = $metadata;
    }

    /**
     * Takes parsed metadata and generates form fields
     *
     * @param  bool   $pre  Determines whether to return HTML or preformatted/encoded HTML
     * @return string Returns HTML form to be echoed
     */
    public function generate( $pre = FALSE )
    {
        $html = '';
        foreach ( $this->formfields as $field ) {
            if ( empty( $field->FieldType ) ) continue;
            if ( in_array( $field->FieldType, $this->supported ) ) {
                $html .= $this->makeHtml( $field );
            } else {
                $html .= '<div><h6>Unsupported field type: <b>'. $field->FieldType .'</b></h6></div>';
            }
            if ( $pre ) $html.= PHP_EOL;
        }
        // $html .= '<input type="hidden" name="pdfForm" value="'. $this->pdfurl .'" />';
        $html .= '<input class="button-primary" type="submit" value="Submit" />';
        // $html .= '</fieldset></form>';
        return ( $pre ) ? htmlentities( $html ) : $html;
    }

    /**
     * Holds the AuPDFtk object for reference
     * @var AuPDFtk
     */
    private $pdfurl;

    /**
     * Holds the metadata for the form fields from PDFtk
     * @var array
     */
    private $formfields;

    /**
     * Holds an array of supported field types
     * @var array
     */
    private $supported = [ 'Text', 'Button' ];

    /**
     * Generates HTML for each supported field type
     * @param  stdClass
     * @return string
     */
    private function makeHtml( $field )
    {
        $html = '';
        if ( property_exists( $field, 'FieldNameAlt') ) $html .= '<label for="'. $field->FieldName .'">'. $field->FieldNameAlt .'</label>';
        else $html .= '<label for="'. $field->FieldName .'">'. $field->FieldName .'</label>';

        // To properly handle the $_POST data, we  all the fields in their own array.
        switch ( $field->FieldType ) {
            case 'Text':
                // $html .= '<label for="'. $field->FieldName .'">'. $field->FieldNameAlt .'</label>';
                $html .= '<input type="text" name="PDFtkFields['. $field->FieldName .']" />';
                break;
            case 'Button':
                foreach ( $field->FieldStateOption as $option ) {
                    $html .= '<input type="radio" name="PDFtkFields['. $field->FieldName .']" value="'. $option.'" />';
                    $html .= '<label class="label-inline" for="">'. $option .'</label>';
                }
                break;
        }
        return $html;
    }
}
